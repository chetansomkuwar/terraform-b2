# profile = "omi"

region = "us-east-1"

ami = "ami-0cdb8266fcd5d3d63"

#instance_count = 2

instance_type = "t2.xlarge"

key_name = "windows_global_key"

tags = {
    Name = "sonarqube-Server"
    Enviorment = "staging"
    Project = "CDEC-B16723"
}