# variable "profile" {
#   type = string
#   description = "here i am providing the profile for my server"
# }

variable "region" {
  type = string
  description = "here i am providing region for my script"
}

# variable "instance_count" {
#   type    = number
#   default = 2  # Adjust the default value as needed
# }

variable "ami" {
  type = string
  description = "here we are providing ami for my project"
}

variable "instance_type" {
  type = string
  description = "here i am providing the server size"
}

variable "key_name" {
  type = string
  description = "here we are providing the key-pair of my server"
}

variable "tags" {
  type = map
  description = "here i am providing the tags to my server"
}