terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.41.0"
    }
  }
}

provider "aws" {
  region = var.region
#   shared_config_files      = ["/home/ubuntu/.aws/config"]
#   shared_credentials_files = ["/home/ubuntu/.aws/credentials"]
#   profile                  = var.profile
}

terraform {
  backend "s3" {
    bucket         	   = "prod-terraform.tfstate-b2025"
    key                = "terraform.tfstate"
    region         	   = "ap-south-1"
    encrypt        	   = true
    dynamodb_table = "terraform-b2025"
  }
}

resource "aws_instance" "my_instance_5" {
#  count = var.instance_count
  ami = var.ami
  instance_type = var.instance_type
  key_name = var.key_name
  tags = var.tags
}